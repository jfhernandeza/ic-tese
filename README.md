[![build status](https://gitlab.com/theses/ic-tese/badges/master/build.svg)](https://gitlab.com/theses/ic-tese/commits/master)

# About

This is a style for the LaTeX thesis at the Institute of Computing, UNICAMP.

# Setup

## Install

The repository should be cloned into your local `texmf` tree.

- In Unix systems:
```bash
mkdir -p ~/texmf/tex/latex/
cd ~/texmf/tex/latex/
git clone git@gitlab.com:theses/ic-tese.git
```

- In macOS system (when latex is installed with mactex):
```bash
mkdir -p ~/Library/texmf/tex/latex/
cd ~/Library/texmf/tex/latex/
git clone git@gitlab.com:theses/ic-tese.git
```

- In other operating systems it may vary depending on your installation. To discover the path you can 
```bash
kpsewhich -var-value TEXMFHOME
```

## Use

Once the style is [installed](#install) in your local `texmf` tree, you don't need to copy these files into your main folder to produce the thesis.

A set of examples are provided inside [`example`](/example/) folder. You can copy the files inside (`.tex` and `.bib`) and setup a directory to work with your thesis.

Note that it is strongly recommended to maintain this style and the content of your thesis separated.

## Use in CI

If you want to use the style into a CI pipeline you need to clone and setup the environment inside the virtual environment. To do so you can configure the environment like:

```bash
LOCALTEX=`kpsewhich -var-value TEXMFHOME`/tex/latex
mkdir -p $LOCALTEX
git clone https://gitlab.com/theses/ic-tese.git $LOCALTEX
```

The `$LOCALTEX` variable will hold the current user local `texmf`, it will create the directory and clone the style into your environment. Note that you need to use the `https` version of the address to access the repository as the virtual machine (most probably) won't have the credentials to access the repository.

# Extras

There is `ic-extas.sty` package that provides extra functionalities. See more information [here](extras/README.md).

# Issues 

Please report any issues or bugs using by openning an [issue in this repository](https://gitlab.com/theses/ic-tese/issues).

# Contributions

We thank the [contributors](/contributors.md) for the project. 

